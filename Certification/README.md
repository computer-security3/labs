# Certificates lab

If you're a teacher and are interested with this lab, please send me an email at rabsil@he2b.be to get the scripts used to generate the data the students need for the last exercice of this lab.

The scripts are not versioned since they give insight on how to solve the lab.