\documentclass[a4paper,11pt]{article}

%\usepackage[showframe]{geometry} %use this if you want to check margins
\usepackage{geometry}
\geometry{centering,total={160mm,250mm},includeheadfoot}

%auxiliairy files
\input{header.tex} %usepackage and configurations
\input{cmds.tex} %user defined commands

\usepackage{todonotes}
\usepackage{dirtree}

%title
\newcommand{\doctitle}{Security}
\newcommand{\docsubtitle}{Digital Certificates}
\newcommand{\tdyear}{2024 -- 2025}
\author{R. Absil}

\newcommand{\deadline}{27 October by 23:59\xspace}
\newcommand{\deadlinesubscription}{20 October by 23:59\xspace}

\begin{document}
\pagestyle{fancy} %displays custom headers and footers

\maketitle

\section*{Description of the project --- Digital Proof of School Enrolment}

\subsection*{Introduction --- Fictitious situation}

You are in charge of an exploratory project to develop a solution for (Digital) Proof of School Enrolment (called DPSE in the following).
The broad idea is to modernise ``student cards''.

Currently, such cards are, commonly, simple paper/plastic printed cards showing a picture, a name, an identifier, and a school name.
It serves two main purposes:
\begin{itemize}
  \item Identify a student in a school, using, for example, the student's roll number.
    This is mostly only of use to members of the school, for example, to quickly check and mark the student's attendance to a test.
  \item Certify that an individual is enrolled in a school.
    This is used by members of the school, for example, to attest an individual's right to be on the school premises, but also by third parties (\eg to allow a reduction fee for a student in small family businesses).
\end{itemize}

Simple printed student cards are useful as simple reminders of a student's ID for school.
However, as they are often extremely easy to forge, they are poorly adequate to certify anything.
To third parties who encounter a lot of different student cards from different schools, the problem is even worse as they cannot be familiar to every single card format.

Other types of cards are using Integrated Circuit Cards (Smart Card) or RFID cards.
Those types of cards imply having increased issuing costs and are not made to be usable by third parties.
Most of the time, only the issuing school has the software means to verify the authenticity of those cards.
Moreover, there exists a lot of different formats of Smart/RFID cards/readers, and low-tech third parties often simply won't possess the necessary hardware (again, think of family businesses offering student reductions).

The proposed solution is inspired by the EU Digital COVID Certificate.
The idea is for the schools to issue student cards as proofs (certificates) in such a way that their authenticity and integrity can be verified and trusted.
Those proofs will be encoded in a QR code that can be either printed on a classic paper student card or stored on the student's smart phone (removing the need to carry a physical card).

Ultimately, the verifier of a DPSE should be able to establish that:
\begin{itemize}
  \item the document has been issued by an authorised entity;
  \item the information presented on the document is authentic, valid, and has not been altered;
  \item the document can be linked to the holder of the DPSE.
\end{itemize}

\subsection*{Digital Proof of School Enrolment (DPSE) document description}

A DPSE stores the following information:
\begin{description}
  \item[\texttt{surname}] Surname of the student.
    It is encoded in the ICAO\footnote{For this homework, you do not need to worry about this ICAO encoding. When you need to write a name, simply use only uppercase latin alphabetic characters and put '<' to separate names. You may assume they are correctly encoded on input.}
    transliterated name format for Machine Readable Travel Documents (Doc 9303), that is, it uses only uppercase latin alphabetic characters and every name separator (space, dash, apostrophe, etc.) are replaced by the character '<'.
  \item[\texttt{givennames}] Given names of the student (ICAO transliterated name format).
  \item[\texttt{rollnumber}] School specific identifier of the student.
  \item[\texttt{country}] Country where the issuing school is established.
  \item[\texttt{school}] Name of the school the student is enrolled in.
  \item[\texttt{validfrom}] Date, in the YYYY-MM-DD format, from which the Proof of School Enrolment is valid.
  \item[\texttt{validto}] Date, in the YYYY-MM-DD format, until which the Proof of School Enrolment is valid.
  \item[\texttt{signature}] A base64 encoded cryptographic signature of this document (details on the signature algorithm are given below).
  \item[\texttt{dscfingerprint}] An uppercase hexadecimal encoded (only alphanumeric) SHA-1 fingerprint of the Digital Signing Certificate (DSC) holding the public key used to sign this DPSE.
\end{description}

A DPSE document is, for this homework, serialized as a text file\footnote{The data layout and serialization scheme proposed here is clearly suboptimal. The goal here is keep things simple.}, made of a line for each field value in the order they appear above. You may find an example of a DPSE document, together with its QR-code encoding\footnote{Simple software, detailled later in the document, allow to easily craft such a code.} at Figure~\ref{fig:dpseex}.

\begin{figure}[ht]
  \centering
  \begin{subfigure}[t]{0.4\textwidth}
  \begin{lstlisting}[breaklines=true, columns=fullflexible]
LE<BLANC
JUSTE
12345
BE
Haute Ecole Bruxelles-Brabant
2021-10-06
2022-09-14
  \end{lstlisting}
    \caption{Unsigned DPSE serialized document}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.55\textwidth}
  \begin{lstlisting}[breaklines=true, columns=fullflexible]
LE<BLANC
JUSTE
12345
BE
Haute Ecole Bruxelles-Brabant
2021-10-06
2022-09-14
MEUCIG61AK6sTjMp7BnM0+5KkKzb1LhzEM+tj60QAuVXz+1IAiEA+w5/lqUBrIFyMRR9DKT13NF3h3jqz3EP8V+55lYYqwQ=
EBFB4F3973EEEC5DFAEB531ABFB70227EE251992
  \end{lstlisting}
    \caption{Serialized DPSE document (signed)}
  \end{subfigure}\\
  \centering
  \begin{subfigure}[t]{0.3\textwidth}
    \includegraphics[width=\textwidth]{pics/12345.pdf}
    \caption{QR-code storing the DPSE (version 9, \(53 \times 53\), Low ECC)}
  \end{subfigure}
  \caption{Example of a Digital Proof of School Enrolment (DPSE)}
  \label{fig:dpseex}
\end{figure}

\subsection*{Public Key Infrastructure --- Generation and verification mechanisms}

\subsubsection*{Preliminary --- Bogus mechanism}

To check that a DPSE is valid, one must not only verify the signature of the document using the public key of the issuer but also that the mentioned public key belongs to a legitimate issuer.
This could be implemented by having each school generate its own cryptographic key pair and let it sign the DPSE of all its students.
The public key must be signed by a Certificate Authority~(CA) that will issue a Digital Certificate (\eg an X.509 certificate) certifying the legitimacy of the school.
The signed certificate is then available on the institutional website, allowing anyone to verify the validity of DPSE documents from students claiming to be enrolled in this particular school.

However, not all schools will have the technical know-how, nor simply have an institutional website to host their public key certificates.
Moreover, one of the desired features of the system is to be able to check the validity of DPSE documents off-line%
\footnote{Off-line validation circumvents the inevitable connectivity problems and speeds-up verifications in a queue.
Online verification also has security and privacy implications.}.
This means that a DPSE reader/verifier must have off-line access to all the public keys of all the schools using DPSE (think all of Europe), making this solution impracticable.

\subsubsection*{Actual mechanism --- Public Key Infrastructure (PKI)}

Here is described the (absolutely basic) Public Key Infrastructure (PKI) for this project.

Each country designates an Education Authority which is responsible to sign the DPSE documents on behalf of the schools on its ground.
To this end, the Education Authority manages its own cryptographic keys and issues Digital Signing Certificates (DSC) to attest ownership of the public signing keys.

A Central Education Authority (\eg at the European level) hosts a central repository containing all the active DSC of each of its member country%
\footnote{The Central Education Authority \emph{should} also play the role of Certificate Authority (CA) and sign the DSC of its members after they send a Certificate Signing Request (CSR).
The Registration Authority (RA) responsibility would also fall upon the Central Education Authority to verify the identity of the entity making the Certificate Signing Request.
This mechanism is \emph{not} implemented in this homework.}.
DSC are stored in the repository in the PEM format and its filename is \verb|DSCFINGERPRINT.pem| where \verb|DSCFINGERPRINT| is the fingerprint of the X.509 certificate encoded as the \texttt{dscfingerprint} field of the DPSE.
This is so that it is easy for a client to fetch a DSC from the value of the \texttt{dscfingerprint} field.
For example, the DSC for the DPSE in the Figure~\ref{fig:dpseex} should be named \texttt{EBFB4F3973EEEC5DFAEB531ABFB70227EE251992.pem} in the public repository.

A School registers itself to the Education Authority of its country using a method left to the Education Authority's discretion.

\paragraph{Issuing of a new DPSE}

When a registered school administration needs to issue a new DPSE (a new student card), it sends the unsigned DPSE to its Education Authority (by other secure means, managed by the Education Authority, e.g., an authenticated API).
The Education Authority then uses the private key of one of its DSC%
\footnote{An Education Authority may well manage multiple cryptographic keys and choose which one to use at its convenience.} %
to sign the DPSE and answers back with the base64 encoded \texttt{signature} field and the \texttt{dscfingerprint} field (uppercase hexadecimal encoded SHA-1 fingerprint of the DSC used to sign the message).
The school (a software) then constructs the signed DPSE document which can be encoded to a QR-code. This QR-code can then be printed on a student card and, together with the DPSE document, be sent to the student's mobile phone for integration in some app.

\paragraph{Verification of a DPSE}

When an entity needs to verify a DPSE, it needs to%
\footnote{Ideally, we could be more paranoid and check more details, but we try to keep it simple in this homework.
Note, for example, that we do not implement any mechanism for certificate revocation.}
\begin{enumerate}
  \item check that the DPSE is not expired;
  \item check that the DSC corresponding to the \texttt{dscfingerprint} field is legitimate to sign the DPSE (check the country of its Education Authority against the country of the school mentioned in the DPSE);
  \item verify the signature of the DPSE using the public key of the DSC mentioned in the DPSE.
\end{enumerate}

\paragraph{Selected cryptographic algorithm --- key types}

All DSC must use the same type of cryptographic keys\footnote{In order to keep it simple.}.
The selected algorithm is the Elliptic Curve Digital Signature Algorithm (ECDSA), with curve P-256 (\texttt{prime256v1}).
This allows for shorter signatures than RSA.

All certificates and keys will be stored in the PEM format.

\section*{Submission}

You will not need to implement a software program for this homework, and instead you will manipulate certificates and keys on the command line, doing things ``manually''. Consequently, for each exercise (detailed later), you will write down the commands you execute and document what they do (to show that you understand what you are doing) in a file called \texttt{commands.md} (in the Markdown format\footnote{See the provided example \texttt{commands.md} for the format.}).

This homework can be realised in pairs (groups of 2 students), and must be submitted online on the poÉSI platform. For this purpose, you need
\begin{itemize}
\item to send us an email detailing your group composition on \deadlinesubscription at the latest;
\item submit your work in an archive under the \texttt{.zip}, \texttt{.7z}, \texttt{.tar.gz} or \texttt{.tar.xz} format%
    \footnote{A \texttt{.rar} archive is \emph{not} accepted, as it is a proprietary format.}, on \deadline at the latest.
\end{itemize}
No delay of any kind shall be tolerated.

Furthermore, projects have to follow the specific structure described in the beginning of the Exercices section.

Projects failing to meet these requirements will not be graded (that is, they will get 0/20).

%\newpage
\section*{Exercises}\label{sec:exos}

The following exercises ask to alternatively place yourself in the role of a certification authority, a regular school, a small business owner, etc. Thus, for each exercise, you have to mention who the actor is, that is, who should execute the commands you give. When the actor needs to be specified, you will write a line \\
\texttt{\#\# Actor: name of the actor}.

You will find an archive on poÉSI with the following structure:
\dirtree{%
.1 secg4-homework3/.
.2 be\_education\_authority/.
.3 private/.
.3 public/.
.3 tmp/.
.2 central\_education\_authority/.
.3 private/.
.3 pubrepo/.
.3 tmp/.
.2 commands.md.
.2 he2b\_school/.
.3 dpse/.
.3 private/.
.3 qr\_dpse/.
.3 tmp/.
}

When you generate files, you have to place them in the correct folder.
\begin{itemize}
  \item The folder \texttt{be\_education\_authority} is to be seen as a server of the Belgian Education Authority.
  \item The \texttt{private/} folder holds files that are securely stored on the server, for each server.
  \item The \texttt{tmp/} folder holds, for each server, the files that should normally be removed from that server as soon as they are not needed.
        But you will \emph{not} delete them.
        Leave them in place so that we (teachers) may look at your temporary files\footnote{You will not get any marks for a piece of work that we cannot verify.}.
        It may be intermediate temporary files that you might generate with your commands.
        This folder will also hold files that are supposedly ``uploaded'' on the server.
        If someone sends a file to the Belgian Education Authority, we suppose it is ``saved'' in the \texttt{be\_education\_authority/tmp/} folder.
  \item The \texttt{be\_education\_authority/public/} folder holds files that should/could be served on the web.
  \item The \texttt{central\_education\_authority/pubrebo/} folder is the central public repository for DSC.
    It is supposedly publicly served to the internet.
  \item The \texttt{he2b\_school/dpse/} folder will be used to store generated DPSE documents.
    The school privately stores the DPSE files to be able to re-print or resend them on a student's request.
  \item The \texttt{he2b\_school/qr\_dpse/} folder will be used to store generated QR-code DPSE files.
\end{itemize}

The commands to execute must be written in a markdown code environment and the first line must be a comment defining the current working directory in the provided directory structure:
\begin{lstlisting}[numbers=none]
```
# cwd: /he2b_school/
my_command 1 2 3
```
\end{lstlisting}

We assume that the commands are run in a \textbf{Bash shell}.

You will essentially use the \texttt{openssl} command, but likely also some common shell commands too, like \texttt{cat}, \texttt{cp}, \texttt{cut}, \texttt{mv}, \texttt{sed}, \texttt{tr}, pipes and redirections.

Here are some hints/recall for some usefull shell tricks:
\begin{itemize}
  \item \verb|echo something > myfile| will overwrite the file \texttt{myfile} with the output of left command (the string ``something'').
  \item \verb|echo something >> myfile| will append the output of left command (the string ``something'') to the file \texttt{myfile}.
  \item \verb:echo "key=value" | cut -d= -f2: will select the \texttt{value} part.
  \item \verb/echo "AA:BC:DE" | tr -d ':'/ will output \texttt{AABCDE}.
  \item \verb:cat myfile | sed -r '4p': outputs the fourth line of the file \texttt{myfile}.
  \item \verb/mycommand | sed -e '$a\'/ will insert a newline character at the of the output if there is not already one.
\end{itemize}

For the usage of \texttt{openssl}, you are on your own\footnote{That is, dig stackoverflow and the internet in general.}.

Note that certificates and keys must not be encrypted using a symmetric password\footnote{In the real world they would (in DES), but this a simplification for this homework.}.

\begin{exo}
  Using \texttt{openssl}, generate a private key for the first Digital Signing Certificate~(DSC) of the Belgian Education Authority, satisfying the requirements of the Public Key Infrastructure~(PKI) described previously.
  Save it locally under the name \verb|be_ea_key.pem|.

  Also, generate an X.509 certificate for the corresponding public key for the Belgian Education Authority.
  Save it locally under the name \verb|be_ea_cert.pem|.
  This certificate should be valid for 10 years.

  Note that the Belgian Education Authority is supposedly based in Belgium, Hainaut, at Charleroi.
  The Organization Name is ``Belgian Education Authority''.
  There is no Unit Name.
  The certificate Common Name should be ``Belgian Education Authority DSC01''.
  The e-mail address is of no importance in this homework.
\end{exo}

\begin{exo}
  \begin{setup}
    we suppose that the X.509 certificate generated previously was sent securely to the Central Education Authority.
    You may copy it to the \verb|tmp/| folder of the Central Education Authority:
  \end{setup}
\begin{lstlisting}[numbers=none]
cp [???]/be_ea_cert.pem central_education_authority/tmp/
\end{lstlisting}

  The Central Education Authority just received the file \verb|be_ea_cert.pem|, it is in its \verb|tmp/| folder.
  The Authority decided to accept this DSC and publish is to the public central repository.

  Store it in the central public repository with the appropriate name (according to the PKI).
\end{exo}

\begin{exo}
  The HE2B School will test the system by generating a DPSE for a test student.

  Generate an \emph{unsigned} DPSE with your information (of one of the group members if you work in group) and save it to \verb|tmp/12345.txt|, where 12345 is your roll number (\ie your matricule):
  \begin{description}
    \item[\texttt{surname}] Your surname.
    \item[\texttt{givennames}] Your given names.
    \item[\texttt{rollnumber}] Your roll number (\textit{matricule})
    \item[\texttt{country}] BE
    \item[\texttt{school}] Haute Ecole Bruxelles-Brabant
    \item[\texttt{validfrom}] 2021-09-15
    \item[\texttt{validto}] 2022-09-14
  \end{description}
\end{exo}

\begin{exo}
  \begin{setup}
    The HE2B School has sent, through a secure channel, the unsigned DPSE to its Education Authority.
    You may copy the unsigned DPSE file to the \verb|tmp/| folder of the Belgian Education Authority:
  \end{setup}
\begin{lstlisting}[numbers=none]
cp he2b_school/tmp/12345.txt be_education_authority/tmp/
\end{lstlisting}

  The Belgian Education Authority received an authenticated request to sign a DPSE from the HE2B School.
  The unsigned DPSE file is in its \verb|tmp/| folder.
  The authority accepts to sign it.

  Compute the signature of the DPSE and store it to \verb|tmp/12345.signature.txt|.

  Compute the fingerprint of the associated DSC X.509 certificate and store it in the \verb|tmp/| folder, with the name \verb|12345.dscfingerprint.txt|
\end{exo}

\begin{exo}
  \begin{setup}
    The Belgian Education Authority answered to the HE2B signing request, through a secure channel.
    You may copy the signature and fingerprint files to the school's \verb|tmp/| folder:
  \end{setup}
  \begin{lstlisting}[numbers=none]
cp be_education_authority/tmp/12345.{signature,dscfingerprint}.txt he2b_school/tmp/
  \end{lstlisting}
  The HE2B School received the signature of its DPSE document and the DSC fingerprint.
  The files are in the \verb|tmp/| folder.

  Assemble the signed DPSE document and store it in the school's \texttt{dpse} storage, with the name \verb|12345.dpse.txt|.

  Optionally\footnote{Really optional, no penalty for not doing it. You won't be able to lift Mjölnir though.}, generate the QR-code, using the \texttt{qrencode} tool (or another):
  \begin{lstlisting}[numbers=none]
cat ???/12345.dpse.txt | qrencode -o qr_dpse/12345.png
  \end{lstlisting}
\end{exo}

\begin{exo}
  In order to test that everything went well, check the validity of the DPSE you just created by verifying the signature.

  Optionally, test decoding the QR-code, using ZBar tools\footnote{\url{http://zbar.sourceforge.net/}}:
  \begin{lstlisting}[numbers=none]
zbarimg qr_dpse/12345.png --raw -q
  \end{lstlisting}
\end{exo}

\begin{exo}
  Your teacher should have provided to your group an archive containing some DSC files together with some DPSE (with the corresponding QR-codes).

  You should have four DSC files, to place in your public central repository.

  You should have five DPSE.
  Check the validity of each of those DPSE.
  For each DPSE, write in the \texttt{commands.md} the roll number of the student and whether or not the DPSE is valid. If not, give briefly the reason why it is not valid.

  For example, you could write something like this:
    \begin{lstlisting}[numbers=none]
452342 VALID

812323 INVALID, because the signature doesn't match.

239765 VALID

[...]
  \end{lstlisting}
\end{exo}

\end{document}

